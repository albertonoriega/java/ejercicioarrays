package ejerciciosarrays;

import javax.swing.JOptionPane;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;



public class EjerciciosArrays {
public static void main(String[] args) {
    //ejercicio1Arrays(); 
    //ejercicio2Arrays();
     //ejercicio3Arrays();
     //ejercicio4Arrasys();
     //ejercicio5Arrays();
     //ejercicio6Arrays();
     //ejercicio7Arrays();
     // ejercicio8Arrays();
     //ejercicio9arrays();
     //mayorColumnaPrimero();
     //practicaArrays();
     //ejercicioMultiplicararrays();
     //arrayLists1();
     //arraylists2();
     //arraylists3();
     //arraydeprimos();
     //ejercicio12arrays();
     ejercicio13arrays();
    
}

public static void ejercicio1Arrays(){
    int i, aprobado=0, j;
    String []aprobados= new String[10];
    float[]Notas={5.8f,6.2f,7.1f,5.9f,3.6f,9.9f,1.2f,10.0f,4.6f,5.0f};
    String [] Nombres = new String[10];
    Nombres[0]="Pedro";Nombres[1]="Ana";Nombres[2]="Luis";
    Nombres[3]="Luis";Nombres[4]="Juan";Nombres[5]="Eva";
    Nombres[6]="Mari";Nombres[7]="Fran";Nombres[8]="Luz";
    Nombres[9]="Sol";
    
    // Hacemos un bucle que recorre el array
    for(i=0;i<Notas.length;i++){
        if(Notas[i]>=5){
            aprobado++; // Contador para calcular el número de aprobados
            aprobados[(aprobado-1)]=Nombres[i]; // Guardamos en un array el nombre de los aprobados
        }
        // Imprimimos para cada posición El nombre y la nota correspondiente
        System.out.println(Nombres[i]+" : "+Notas[i]);
    }
    //Imprimimos el numero de aprobados
    System.out.println("Aprobados : "+aprobado);
    // Imprimimos el nombre de los aprobados. Como ponemos print y no println, nos salen todos los nombres en la misma línea
    System.out.print("Aprobados : ");
    for (j=0;j<aprobado;j++){
        // Para imprimir los nombres de los aprobados usamos el array que hemos creado de aprobados
    System.out.print(aprobados[j]+" ");
}
}

public static void ejercicio2Arrays(){
    // creamos un array de numeros enteros de dimension 100
    int [] numeros = new int [100];
    int i, j=1, suma=0, media=0;
    // recorremos el array desde la posicion 0 al 99 
    for(i=0;i<numeros.length;i++){
        // Para la posición i del array asignamos el valor j
    numeros[i]=j;
    j++; // sumamos 1 al contador por cada interacción del bucle
        suma=suma+j; // Al contador suma le sumamos cada valor de j
    }
    // calculamos la media
    media=suma/numeros.length;
    // Imprimimos tanto la suma como la media
    System.out.println("La suma de todos los numeros es: "+suma);
    System.out.println("La media de todos los numeros es: "+media);
}

public static void ejercicio3Arrays(){
    String texto;
    int i, largoFrase,j=0;
    // Pedimos al usuario que introduzca una frase
    texto = JOptionPane.showInputDialog("Introduce frase");
    // Obtenemos la longitud de la frase para saber la dimension del array de char
    largoFrase= texto.length();
    // Inicializamos el Array de char
    char [] c = new char[largoFrase];
    // impresión de la frase introducida
    System.out.println(texto);
    // Bucle para dar la vuelva a la frase e imprimirla
    for(i=largoFrase-1;i>(-1);i--){
        // Guardamos en el array de char, caracter a caracter.
        // Para dar la vuelta a la frase :
        // el array los recorremos de la posicion 0 a la ultima
        // la frase la recorremos del último caracter al primero
        c[j]=texto.charAt(i); 
        // Imprimimos la frase invertida
        System.out.print(c[j]);
        j++;
    
}
   
}

public static void ejercicio4Arrasys(){

String textoNum, textoNombre, textoNota;
int num, i,j, nota;
// Pedimos el numero de alumnos
textoNum = JOptionPane.showInputDialog("Número de alumnos");
// Convertimos el texto introducido a entero
num = Integer.parseInt(textoNum);
//Creamos dos arrays de tipo String de longitud igual al numero de alumnos inroducido (Notas y nombres)
int []notas = new int[num];
String []nombres = new String [num];
// Bucle que pide un nombre y una nota. Lo pide tantas veces como el numero de alumnos introducido
for (i=0;i<num;i++){
textoNombre = JOptionPane.showInputDialog("Nombre de alumno");
nombres[i]=textoNombre; // Guardamos el nombre introducido en el array de nombres
textoNota = JOptionPane.showInputDialog("Nota del alumno");
nota= Integer.parseInt(textoNota); // Guardamos la nota introducida en el array de notas
notas[i]=nota;
}
// Bucle para imprimir el nombre del alumno junto a su nota
for(j=0;j<nombres.length;j++){
    System.out.println("El alumno "+ nombres[j]+" ha sacado un "+notas[j]);
}
}


    public static void ejercicio5Arrays() {
        String textoNum;
        int i, j = 0, k, contador = 0;
        textoNum = JOptionPane.showInputDialog("Introduce número");
        // Generamos un array de char cuya dimensión es el numero de cifras del numero introducido
        // Si el numero es 100 => 3 ; Si es 2580 => 4 ; etc
        char[] numero = new char[textoNum.length()];
        // Bucle que guarda cada cifra del numero introducido en una posicion del array numero
        for (i = 0; i < textoNum.length(); i++) {
            numero[j] = textoNum.charAt(i);
            j++; 
            //System.out.println(numero[i]);
        }
        // Imprimimos el array, en lugar de imprimirlo con un bucle lo hacemos usando el metodo toString
        // Por ejemplo, 202 te lo imprimiría => [2 , 0 , 2]
        System.out.println(Arrays.toString(numero));
        // Asignamos a una variable el numero de cifras del numero introducido 
        k = textoNum.length();
        // Bucle que compara la última cifra con la primera
        // Por ejemplo 256345
        // En la primera interacción compara posicion [0]=2 con posicion [5]=5
        // En la siguiente interacción compara posicion [1]=5 con posicion [4]=4
        // En la siguiente interacción compara posicion [2]=6 con posicion [3]=3
        for (i = 0; i < (k / 2); i++) {
            // Condicional donde si los dos numeros comparados no son igual sumas 1 al contador
            if (numero[i] != numero[k - 1]) {
                contador++; 
            }
            k--;
        }
        // Condicional que imprime si es capicua o no
        if (contador == 0) {
            System.out.println(textoNum + " es capicua");
        } else {
            System.out.println(textoNum + " no es capicua");
        }
    }


public static void ejercicio6Arrays(){
    int i, contador=0;
    char vocalChar;
String texto, vocal;
texto=JOptionPane.showInputDialog("Introduce frase");
vocal=JOptionPane.showInputDialog("Introduce vocal");
// Convertimos la vocal que es un String en un char
vocalChar=vocal.charAt(0);
// Como vamos a comparar un texto con un caracter, pasamos todo el texto a minusculas
// ya que si el caracter fuera el mismo pero estuviera en mayusculas no lo tendrñia en cuenta
// Además, convertimos el texto en un array de caracteres
char c []=texto.toLowerCase().toCharArray();
// Hacemos un bucle en el que comparamos cada caracter de la frase introducida con la vocal pasada
for (i=0;i<texto.length();i++){
    if(vocalChar==(c[i])){
        contador++; // Cada vez que haya una coincidencia el contador suma 1
    }
    
}
// Mostramos cuantas coincidencias ha habido con la vocal introducida
    System.out.println("En la frase introducida hay "+contador+" "+vocal);
}

public static void ejercicio7Arrays(){
    String textoNumArray, textoNumCambio;
    int numArray, i, numCambio, c=0;
    // Pedimos al usuario el tamaño del array y el numero a cambiar
    textoNumArray=JOptionPane.showInputDialog("Introduce tamaño del array");
    numArray=Integer.parseInt(textoNumArray); // conversión de String a int
    textoNumCambio=JOptionPane.showInputDialog("Introduce numero a cambiar");
    numCambio=Integer.parseInt(textoNumCambio); // conversión de String a int
    // Creamos un array de ints cuyo tamaño será el numero introducido por el usuario
int [] num = new int[numArray];
    System.out.println("Array inicial");
    // Rellenamos el array, cada posición tendrá un número aleatorio entre el 0 y el 9
for (i=0;i<numArray;i++){
    num[i]=(int)(Math.random()*9+1);
    System.out.print(num[i]+ " "); // Imprimimos cada número del array
}
System.out.println("\nArray modificado");
for(i=0;i<numArray;i++){
if(num[i]==numCambio){ // comparamos cada posición del array de nñumeros con el numero a cambiar
    c++; // contador que muetsra el numero de veces que cambiamos un numero
    num[i]=0; // Si el numero a cambiar está en el array lo cambiamos por un 0
    
}
System.out.print(num[i]+ " ");
}
// Imprimimos las veces que estaba el numero a cambiar en el array
    System.out.println("\n El numero "+numCambio+" aparece "+c+" veces");

}

public static void ejercicio8Arrays(){
    int i, j, mes,a=12, b=30; // matriz axb = 12x30
    String textoMes;
    // creamos la matriz temperaturas de doubles de dimension 12x30
double temperaturas [][]= new double[a][b];
// Creamos un bucle en el que introducimos una temperatura entre 0 y 35 para cada dia de cada mes
for(i=0;i<a;i++){ // el primer for va recorriendo los meses del 1 al 12 (de la posicion 0 a la 11 al ser un array)  
    for(j=0;j<b;j++){ // el segundo for va recorriendo los dias del 1 al 30 ( de la posicion 0 a la 29 al ser un array)
        temperaturas[i][j]=(Math.random()*35); 
    }
}
textoMes=JOptionPane.showInputDialog("Introduce mes");
mes=Integer.parseInt(textoMes);
// Para el mes introducido por el usuario imprimimos las temperaturas de cada dia
// como el mes no varía lo dejamos fijo [mes-1] e iteramos el array de los días
for (j=0;j<b;j++){
            System.out.println(temperaturas[mes-1][j]);
}
    
   
}


public static void ejercicio9arrays(){
    //Generar dos matrices cuadradas aleatorias de dimension pedida al usuario y scaar una tercera con la suma de las dos anteriores
    int numero, i, j, numeroMatriz1, numeroMatriz2;
    String textoNum;
int matriz1[][];
int matriz2[][];
int matrizSuma[][];
// Pedimos la dimension de la matriz cuadrada al usuario
textoNum=JOptionPane.showInputDialog("Introduce la dimensión de la matriz");
numero=Integer.parseInt(textoNum);
// Una vez que tenemos la dimensión inicializamos dichas matrices con esa dimension
matriz1= new int [numero][numero];
matriz2= new int [numero][numero];
matrizSuma = new int [numero][numero];
// Rellenamos las matrices 1 y 2 con numeros enteros aleatorios entre el 1 y el 9
for (i=0;i<numero;i++){
    for(j=0;j<numero;j++){
        numeroMatriz1=(int)(Math.random()*9+1);
        numeroMatriz2=(int)(Math.random()*9+1);
matriz1[i][j]=numeroMatriz1;
matriz2[i][j]=numeroMatriz2;
// A su vez, en la matriz suma calculamos la suma de los dos numeros para cada posicion
matrizSuma[i][j]=numeroMatriz1+numeroMatriz2;

        }
    
}
// OPCION 1 DE IMPRIMIR MATRICES
    System.out.println("Matriz 1");
for (i=0;i<numero;i++){
    for(j=0;j<numero;j++){
    System.out.print(matriz1[i][j]+ " "); // print y  o println para que hasat que no cambie de fila no haga un salto de linea
}
    System.out.println(); // Una vez que acaba la fila, hace el salto de linea e imprime la siguiente fila
}
   System.out.println("Matriz 2");
for (i=0;i<numero;i++){
    for(j=0;j<numero;j++){
    System.out.print(matriz2[i][j]+ " ");
}
    System.out.println();
}
System.out.println("Matriz Suma");
for (i=0;i<numero;i++){
    for(j=0;j<numero;j++){
    System.out.print(matrizSuma[i][j]+ " ");
}
    System.out.println();
}
// Otra forma de imprimir las matrices bidimensionales
System.out.println("Matriz 1");
for(int[] ints: matriz1){
System.out.println(Arrays.toString(ints));
}
System.out.println("Matriz 2");
for(int[] ints: matriz2){
System.out.println(Arrays.toString(ints));
}
System.out.println("Matriz Suma");
for(int[] ints: matrizSuma){
System.out.println(Arrays.toString(ints));
}

}

public static void ejercicioMultiplicararrays(){
    //Generar dos matrices cuadradas aleatorias de dimension pedida al usuario y scaar una tercera con la suma de las dos anteriores
    int numero, i, j,k , numeroMatriz1, numeroMatriz2;
    String textoNum;
int matriz1[][];
int matriz2[][];
int matrizMultiplicacion[][];
textoNum=JOptionPane.showInputDialog("Introduce la dimensión de la matriz");
numero=Integer.parseInt(textoNum);
matriz1= new int [numero][numero];
matriz2= new int [numero][numero];
matrizMultiplicacion = new int [numero][numero];
for (i=0;i<numero;i++){
    for(j=0;j<numero;j++){
        numeroMatriz1=(int)(Math.random()*10+1);
        numeroMatriz2=(int)(Math.random()*10+1);
matriz1[i][j]=numeroMatriz1;
matriz2[i][j]=numeroMatriz2;
        }
    }
    System.out.println("Matriz 1");
for (int[] ints : matriz1) {
            System.out.println(Arrays.toString(ints));
        }
System.out.println("Matriz 2");
for (int[] ints : matriz2) {
            System.out.println(Arrays.toString(ints));
        }

// calculamos la matriz producto
for (i=0;i<numero;i++){
    int c=0;
    for(j=0;j<numero;j++){
        for(k=0;k<numero;k++){
        matrizMultiplicacion[i][j]+=matriz1[i][k]*matriz2[k][j];
        }
}
    
    
    
    }
    


System.out.println("Matriz producto");
for (int[] ints : matrizMultiplicacion) {
            System.out.println(Arrays.toString(ints));
        }




}

    public static void mayorColumnaPrimero() {
        String textoFilas, textoColumnas, textomatriz;
        int numFilas, numColumnas, i, j, numeroMatriz, suma, columnaMayor = 0;
        int matriz1[][];
        int[] sumaColumnas;
        int[] primeraColumna;
        // Pedimos al usuario el numero de filas y columnas
        textoFilas = JOptionPane.showInputDialog("Introduce número de filas");
        numFilas = Integer.parseInt(textoFilas);
        textoColumnas = JOptionPane.showInputDialog("Introduce número de columnas");
        numColumnas = Integer.parseInt(textoColumnas);
        // Inicializamos la matriz con las dimensiones introducidas
        matriz1 = new int[numFilas][numColumnas];
        // Creamos un array de dimension = al numero de filas
        primeraColumna = new int[numFilas];
        // Bucle para rellenar la matriz, en este caso cada numero lo mete el usuario
        for (i = 0; i < numFilas; i++) {
            for (j = 0; j < numColumnas; j++) {
                // ponemos i+1 y j+1 porque la posicion 0 equivale a la fila 1 y columna 1
                textomatriz = JOptionPane.showInputDialog("Introduce valor de la fila " + (i + 1) + " y columna " + (j + 1));
                numeroMatriz = Integer.parseInt(textomatriz);
                matriz1[i][j] = numeroMatriz;
                // Guardamos en el array primeraColumna todos los valores de la primera columna (la fila varía, la columna siempre será la posicion 0
                primeraColumna[i] = matriz1[i][0];
            }

        }
        // Imprimimos la matriz y el array con la primera columna
        System.out.println("Matriz ");
        for (int[] ints : matriz1) {
            System.out.println(Arrays.toString(ints));
        }
        System.out.println("Array de la primera columna");
        System.out.println(Arrays.toString(primeraColumna));


// SUMA DE COLUMNAS
// Generamos un array en el que obtenemos la suma de cada columna
sumaColumnas = new int[numColumnas];
    for (j = 0; j < numColumnas; j++) {
        suma = 0; // Cada vez que acaba de recorrer una columna, suma vuelve a valer 0

        for (i = 0; i < numFilas; i++) {
            // vamos sumando los valores de cada columna y los guardamos en la variable suma
            suma = suma + matriz1[i][j];
            // Al array sumaColumnas le guardamos el valor de la suma de cada columna en una posicion del array
            sumaColumnas[j] = suma;
        }
    }
    // Imprimimos el array con la suma de las columnas
    System.out.print("La suma de cada columna es:");
    System.out.println(Arrays.toString(sumaColumnas));


// Obtenemos el numero de la columna mayor

int total=0; 
//Creamos un bucle que recorre el array con las sumas de las columnas para sacar la columna mayor
for (i=0;i<sumaColumnas.length;i++){
    if (sumaColumnas[i]>total) { // si la suma de la columna es mayor que la anterior, guarda el numero de la columna en columnaMayor
        total=sumaColumnas[i];
        columnaMayor=i;
    }
    
}
    System.out.println("La columna mayor es: "+columnaMayor);
    
    
// GUARDAR COLUMNA MAYOR
// Generamos un nuevo array para guardar los valores de la columna mayor
int colMayor[]= new int[numFilas];
for(i=0;i<numFilas;i++){
for (j=0;j<numColumnas;j++){
    // solo iteramos filas, la columna no varía
    colMayor[i]=matriz1[i][columnaMayor];
}
}
// Imprimimos el array de la columna mayor
System.out.println(Arrays.toString(colMayor));

// INTERCAMBIAR COLUMNAS
if(columnaMayor!=0){ // Si la columna mayor es la 0 no hayq ue hacer nada
for(i=0;i<numFilas;i++){
    for(j=0;j<numColumnas;j++){
    matriz1[i][0]=colMayor[i]; // Los numeros de cada fila de la columna 1 se cambian por los de la columna mayor
    matriz1[i][columnaMayor]=primeraColumna[i]; //Los numeros de cada fila de la columna mayor se cambian por los de la columna 1
    }
}
}
// Imprimimos la matriz resultante
System.out.println("Matriz ");
        for (int[] ints : matriz1) {
            System.out.println(Arrays.toString(ints));
        }
}


public static void practicaArrays(){
int num, i, pedirNum, contador=0;
String textoNum, pedir;
int  numeros[];

textoNum=JOptionPane.showInputDialog("Introduce longitud del array");
num=Integer.parseInt(textoNum);
numeros= new int[num];
for (i=0;i<num;i++){
    numeros[i]=(int)(Math.random()*9+1);

}
//System.out.println(Arrays.toString(numeros));
pedir=JOptionPane.showInputDialog("Introduce numero a comprobar");
pedirNum=Integer.parseInt(pedir);
for (i=0;i<num;i++){
    if(numeros[i]==pedirNum){
        contador++;
    }
}
if(contador!=0){
    System.out.println("CORRECTO");
    System.out.println("El número de veces que está tu número en el array es: "+contador);
} else {
    System.out.println("FALLO");
}

}


// ArrayList

public static void arrayLists1(){
    
    int  num, suma=0, promedio;
    String textoNum;      
    ArrayList<Integer> numeros = new ArrayList<Integer>();
    do {        
        textoNum=JOptionPane.showInputDialog("Introduce numero");
        num= Integer.parseInt(textoNum);
        numeros.add(num);
        suma+=num;
    } while (num!=0);
    promedio=suma/(numeros.size()-1);
    System.out.println(numeros);
    System.out.println("La suma de todos los numeros es: "+suma);
    System.out.println("El promedio es : "+promedio);
}

public static void arraylists2 (){

    int numeroAlumnos, i, altos=0, bajos=0;
    String textoNum;
    double altura, suma=0, alturaMedia ;
    ArrayList <Double> alturasClase = new ArrayList<>();
    textoNum=JOptionPane.showInputDialog("Introduce el número de alumnos");
    numeroAlumnos=Integer.parseInt(textoNum);
    for (i=0;i<numeroAlumnos;i++){
        textoNum=JOptionPane.showInputDialog("Introduce la altura del alumno "+i);
    altura=Double.parseDouble(textoNum);
        alturasClase.add(altura);
        suma+=altura;
    }
    System.out.println(alturasClase);
    alturaMedia=suma/alturasClase.size();
    System.out.println("La altura media de la clase es: "+alturaMedia);
    for(i=0;i<alturasClase.size();i++){
    if(alturasClase.get(i)>=alturaMedia){
        altos++;
    } else {
        bajos++;
    }
    }
    System.out.println("Número de alumnos más altos que la altura media: "+altos);
    System.out.println("Número de alumnos más bajos que la altura media: "+bajos);
    
}

public static void arraylists3 (){
    int opcion, i, posicion, c=0;
    double numero;
    String texto, textoNumero, textoPosicion;
    ArrayList <Double> menu = new ArrayList<>();
    do { 
    texto= JOptionPane.showInputDialog("[1]Agregar \n [2]Buscar \n [3] Modificar elemento \n [4] Eliminar elemento \n [5] Insertar elemento \n [6] Mostrar elementos \n [7] Orden ascendente \n [8] Orden descendente [9] Salir \n Introduzca opción (1-9)");
    System.out.println(texto);
    opcion=Integer.parseInt(texto);
           
            
    switch (opcion) {
        case 1 :
            textoNumero=JOptionPane.showInputDialog("Número a agregar");
            numero=Double.parseDouble(textoNumero);
            menu.add(numero);
            break;
        case 2 :
            textoNumero=JOptionPane.showInputDialog("Número a buscar");
            numero=Double.parseDouble(textoNumero);
            for (i=0;i<menu.size();i++){
                if (numero==menu.get(i)){
                    c++;
                }
            }
                if(c==0){
                     System.out.println("El numero no está en la lista");
                } else {
                    System.out.println("El número está en la lista "+c+" veces");
                }
            
            break;
        case 3 :
            textoPosicion=JOptionPane.showInputDialog("Posición del elemento a modificar");
            posicion=Integer.parseInt(textoPosicion);
            if (posicion<(menu.size()-1)){
            textoNumero=JOptionPane.showInputDialog("Número nuevo");
            numero=Double.parseDouble(textoNumero);
            menu.set(posicion, numero);
            } else {
                System.out.println("La lista no es lo suficientemente grande para la posición indicada");
            }
            break;
        case 4 :
            textoPosicion=JOptionPane.showInputDialog("Posición del elemento a eliminar");
            posicion=Integer.parseInt(textoPosicion);
            if(posicion<(menu.size()-1)){
            menu.remove(posicion);
            } else {
                System.out.println("La lista no es lo suficientemente grande para la posición indicada");
            }
            break; 
        case 5 :
            textoPosicion=JOptionPane.showInputDialog("Posición del elemento a insertar");
            posicion=Integer.parseInt(textoPosicion);
            if(posicion<(menu.size()-1) || posicion==0){
            textoNumero=JOptionPane.showInputDialog("Número nuevo");
            numero=Double.parseDouble(textoNumero);
            menu.add(posicion, numero);
            } else {
                System.out.println("La lista no es lo suficientemente grande para la posición indicada");
            }
            break;
        case 6 :
            System.out.println(menu);
            break;
        case 9: 
            break;
        default:
            System.out.println("Opcion invalida");
    }
}     while (opcion!=9);
    
}

public static void arraydeprimos() {
    int i, num, numero1, numero2, mayor=0;
    String textoNum;
    // Pedimos un numero por teclado. En lugar de pedirlo en el codigo del ejercicio
    // generamos una funcion externa para pedir el numero
    num= pedirNumero();
    // Generamaos un array cuya longitud sera el numero introducido
    int [] numeros = new int [num];
    
    // Pedimos dos numeros, maximo y minimo al usuario
    textoNum=JOptionPane.showInputDialog("Numero minimo");
    numero1=Integer.parseInt(textoNum);
    textoNum=JOptionPane.showInputDialog("Numero maximo");
    numero2=Integer.parseInt(textoNum);
    // iteraos desde la posicion 0 del array hasta la ultima
    for (i=0;i<num;i++){
        // para cada posición del array generamos un numero aleatorio comprendido entre
        // numero maximo y numero minimo introducido
        numeros[i] = (int)(Math.random()*(numero2-numero1+1)+numero1);
        // Comprobamos si cada numero aleatorio generado es primo llamando al metodo primo
         if (i==0 && !primo(numeros[i] )){
                        
       // Si en la posición 0 el numero no es primo, hacemos que i vuelva a valer 0 porque el numero no nos vale
                i=0;
          }
              if (!primo(numeros[i] ) && i>0){ {
       // Si la posición no es la 0 y el numero no es primo, restamos uno a i y volvemos a generar otro numero para esa posición    
                i--;
             }
        }
        // Para cada número primo generado, comprobamos cual es el mayor y lo guardamos en una variable
        if(numeros[i]>mayor){
            mayor=numeros[i];
        }
        
    }
    // Impresiín del array y del numero mayor
    System.out.print ("Tu array es: ");
    System.out.println(Arrays.toString(numeros));
    System.out.println("El numero mayor es: " + mayor);
      
   }

    // Metodo que le pide al usuario un numero
    //en nuestro caso para calcular la longitud del array
 public static int  pedirNumero() {
    String  textoNum;
    int num;
    textoNum=JOptionPane.showInputDialog("Introduce numero");
    num=Integer.parseInt(textoNum);
    return num;
    }
 
 // Metodo que te caalcula si un numero es primo
 public static boolean primo (int numero){
 
     int  i, divisible=0;
         // Un numero es primo si solo tiene dos divisibles ( entre si mismo y entre 1)
        for(i=1;i<=numero;i++){
        // Iteramos desde 1 hasta el numero a comparar
            if(numero%i==0){
        // si el resto de la division es 0 sumamos 1 a la variable divisble        
                divisible++;
                
            }
        }
        //Si divisible es 2, el numero es primo. Si es mayor, no lo es
        // Como hemos creado que nuestro metodo devuelva un booleano, devolvemos true o false
        if(divisible==2){ // Si es primor devolvemos true
            return true;
        } else {  // si no es primero devolvemos false
            return false;
            
        }
 }


  public static void ejercicio12arrays (){
    int num, i, j=0, numero;
    // pedimos un numero al usuario a través de un método
    num=pedirNumero();
    // Inicializamos dos arrays cuya longitud es el numero introducido
    int [] numeros = new int [num];
    int [] acabados = new int [num];
    // Rellenamos el array de numeros con numeros aleatorios entre e1 y el 300
    for (i=0;i<num;i++){
        numeros[i]=(int)(Math.random()*300+1);
    }
    // Imprimimos el array numeros
   System.out.println(Arrays.toString(numeros));
   
   // Pedimos un numero al usuario que esté comprendido entre el 0 y el 9
   do {         
         numero= pedirNumero();
    } while (numero<0 || numero >9);

   for(i=0;i<num;i++){
         // Para obtener la ultima cifra de un numero sería => numero%10
 // comprobamos que cada ultima cifra de cada numero coincide con el numero introducido por el usuario
         if(numeros[i]%10==numero){ 
           // Guardamos en el array acabados los numeros cuya última cifra coincide con el numero
           
           acabados[j]=numeros[i];
           j++;
         }
       }
   // Imprimimos el array de acabados
      System.out.print("Array acabados:");
   System.out.println(Arrays.toString(acabados));
      System.out.print("El array de acabados sera: ");
   for (i=0;i<j;i++){
       System.out.print(acabados[i]+ ", ");
   }
  }

  
  public static void ejercicio13arrays() {
      
      String textoNum, nombre;
      int i;
      double nota;
      double[] notas = new double[10];
      String[] nombres = new String[10];

      // Buble para rellenar los arrays de nombres y notas
      for (i = 0; i < nombres.length; i++) {
          textoNum = JOptionPane.showInputDialog("Introduce nota");
          nota = Double.parseDouble(textoNum);
          // Si i=0 No te deja meter una nota que no esté entre 0 y 10
              if (i==0 && (nota > 10.0 || nota < 0 )) {
                  do {                      
                      System.out.println("Nota introducida no valida");
                      
                  } while (nota <10 && nota >  0);
            // Si i es distino de 0 si la nota no es correcta, no la guarda      
              } else if (i>0 && nota > 10 || nota < 0) {
              System.out.println("Nota introducida no valida");
              i--; // Si la nota no es correcta vuelve a la i anterior
          } else{
                  // Si la nota es correcta, la guarda en el array y te pide el nombre del alumno
                  notas[i] = nota;
                  nombre = JOptionPane.showInputDialog("Introduce nombre del alumno");
                  nombres[i] = nombre;
              }
          }
            
      System.out.println(Arrays.toString(nombres));
      System.out.println(Arrays.toString(notas));
       
      // Bucle para rellenar el array Resultado en funcion de la nota
      String[] resultado = new String[10];
      for (i=0;i<nombres.length;i++){
          if (notas[i] > 0 && notas[i]<5) {
              resultado[i]= "Suspenso";
          } else if (notas[i] >= 5 && notas[i]<7){
              resultado[i]= "Bien";
          } else if (notas[i] >= 7 && notas[i]<9){
            resultado[i]= "Notable";
          }else {
              resultado[i]= "Sobresaliente";
          }
          }
      
      // Bucle que imprime Alumno, nota y resultado
      for (i=0;i<nombres.length;i++){
          System.out.print("Alumno: "+nombres[i]);
          System.out.print(" Nota: "+notas[i]);
          System.out.print(" Resultado: "+resultado[i]);
          System.out.println("");
      }
      
  }
}


